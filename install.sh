#!/usr/bin/bash

echo "Installing R-packages: dplyr, ggplot2, reshape, knitr"
R --vanilla < scripts/install.R

echo "Installing required python packages"
pip3 install pandas xlwt xlrd --user
