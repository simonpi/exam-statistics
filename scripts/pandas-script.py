# coding: utf-8
import pandas as pa
data = pa.read_csv('___grades-template.txt',
                   sep=None,
                   engine='python',
                   encoding='ISO-8859-1')
grades = pa.read_csv('grades-out.csv',
                     encoding='ISO-8859-1',
                     sep=',')
sel_grades = grades[['Number', 'Grade']]
out = pa.merge(data.drop('Grade', 1),
               sel_grades,
               on=('Number'))
out = out[data.columns]

out.to_csv('edoz-final.txt', sep='\t', encoding='ISO-8859-1', index=False)
