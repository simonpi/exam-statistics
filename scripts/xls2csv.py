#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Convert xls to csv in ISO-8859-1 encoding. Unfortunately python pandas does not
support odt.
"""

import pandas as pa
import sys
import argparse
import os

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('src', help='path/to/xls')
    parser.add_argument('dst', help='output')
    args = parser.parse_args()

    if not os.path.exists(args.src):
        raise Exception('Could not read from file ' + str(args.src))

    data = pa.read_excel(args.src)
    data.to_csv(args.dst, index=False, encoding='ISO-8859-1', sep=',')
