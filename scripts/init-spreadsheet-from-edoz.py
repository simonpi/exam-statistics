#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Exctract the columns 'Legi-Number', 'Last Name', 'First Name' from the file
Grades*txt (the one exported from edoz). Store result as xls.
"""

# to_excel needs python-xlwt

import pandas as pa
import argparse
import os

selected_columns = ['Number', 'Last Name', 'First Name', 'Direction']

# header_de = [
#     'Id', 'Familienname', 'Vorname', 'Nummer', 'Sekretariat', 'Fachrichtung',
#     'Rep.', 'Note', '+ / - (Notentendenz)', '* (Abbruch/nicht erschienen)',
#     'Prüfsumme'
# ]
# header_en = [
#     'Id', 'Last Name', 'First Name', 'Number', 'Administration Office',
#     'Direction', 'Rep.', 'Grade', '+ / - (Grade Trend)', '* (dropout/no show)',
#     'Checksum'
# ]

# skip unneeded columns
header_de = [
    'Familienname', 'Vorname', 'Nummer', 'Fachrichtung'
]

header_en = [
    'Last Name', 'First Name', 'Number', 'Direction'
]


translation = {
    'Id': 'Id',
    'Familienname': 'Last Name',
    'Vorname': 'First Name',
    'Nummer': 'Number',
    'Sekretariat': 'Administration Office',
    'Fachrichtung': 'Direction',
    'Rep.': 'Rep.',
    'Note': 'Grade',
    '+ / - (Notentendenz)': '+ / - (Grade Trend)',
    '* (Abbruch/nicht erschienen)': '* (dropout/no show)',
    'Prüfsumme': 'Checksum'
}


def extract_columns(args):
    """

    """
    try:
        edoz_data = pa.read_csv(args.src,
                                sep=None,
                                encoding='ISO-8859-1',
                                engine='python')
    except:
        raise Exception('Could not read from csv-file ' + str(args.src) +
                        str('\nCheck that the file exists and is ISO-8859-1 encoded'))

    if 'Familienname' in edoz_data.columns:
        # there is a tab past there last column => pandas reads an additional empty
        # column (filled with nan) get rid of this additional column
        # edoz_data = edoz_data[header_de]
        # # translate headers to EN
        # assert(all([(i in header_de or i in header_en) for i in edoz_data.columns]))
        # new_col_names = [translation[c] for c in edoz_data.columns]
        # edoz_data.columns = new_col_names
        raise Exception('Use the english version of edoz to export the student list. \n \
        E.g. click on _English_ during login.')
    elif 'Last Name' in edoz_data:
        # there is a tab past the last column => pandas reads an additional empty
        # column (filled with nan) get rid of this additional column
        edoz_data = edoz_data[header_en]
    else:
        raise Exception(
            'Something with the column-headers is wrong (expected to find Last Name). Check your input file %s.'
            % args.src)

    grade_columns = edoz_data[selected_columns].copy()
    for i in range(1,5):
        grade_columns['A%d' % i] = ''

    if not os.path.exists('marks.xls') or args.force:
        grade_columns.to_excel(
            'marks.xls', encoding='ISO-8859-1', index=False)
    else:
        raise Exception(
            './marks.xls exists. Delete or rename it and try again.')

    if args.to_csv:
        if not os.path.exists('marks.csv') or args.force:
            grade_columns.to_csv(
                'marks.csv', sep=',', encoding='ISO-8859-1', index=False)
        else:
            raise Exception(
                './marks.csv exists. Delete or rename it and try again.')


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        'src', help='exported edoz file (Grades|Noten)*txt')
    parser.add_argument(
        '-f',
        '--force',
        help='overwrite *csv,xls without asking',
        action='store_true')
    parser.add_argument('--to-csv', help='Also save as csv (ISO-8859-1)', action='store_true')
    args = parser.parse_args()

    extract_columns(args)
