This is intended as a user-friendly (and easy to customize) alternative to the previous exam statistics script.

.. role:: bash(code)
   :language: bash


Installation
============

Run :bash:`./install.sh`. This will download and compile the required R-packages
and install `pandas <http://pandas.pydata.org/>`_ and `xlwt
<https://pypi.python.org/pypi/xlwt>`_ for python3 (via pip).

You have to clone or download the git repo first:

.. code:: shell

          git clone git@gitlab.math.ethz.ch:A-SAM/new-exam-statistics.git

or

.. code:: shell

          git clone https://gitlab.math.ethz.ch/A-SAM/new-exam-statistics.git


Usage
=====

1. Login to edoz (use the English version), go to ``Grades/Results > Grades`` and
   click on ``Export data``. Unzip the contents (below called GRADES_FILE.txt) into
   the current directory, e.g. the directory containing install.sh.
2. Open a shell (cd to the directory containing the scripts) and type:
   :bash:`./1-prepare-to-enter-marks GRADES_FILE.txt`. This will create a file ``marks.xls``
3. Open ``marks.xls`` in a spreadsheet editor (for example localc) and enter
   the number of points per task for each student (Columns A1..An). Add or remove
   columns for tasks as needed. Save as Excel document to ``marks.xls``.
4. Open the file ``statistics.Rnw`` in a text editor and fill in the variables in the
   block "User Settings". That means the lecture title, the interpolation points for
   the grading scale and the maximal number of points per task.
5. Open a shell and type :bash:`./2-generate-stats-and-edoz-upload`. This will generate the
   files ``statistics.pdf`` and ``edoz-final.txt``
6. Check the grades of a few students manually to make sure that the results in
   ``edoz-final.txt`` are correct.
7. Upload ``edoz-final.txt`` to edoz by clicking on ``Import Data``.

Preview
=======

.. figure:: preview.png



Hints
=====

The R code contained in the `noweb <https://en.wikipedia.org/wiki/Noweb>`_ file
``statistics.Rnw`` will be extracted during execution to ``statistics.R``. The latter
file is not executed by the script, thus changing it has no effect on the grades in
``edoz-final.txt`` or the pdf.

Customization of the plots should be straight forward (e.g. some simple
R/ggplot2 commands). Have a look at the following packages and the file
``statistics.Rnw``.

Links
-----

- Plots: `ggplot2 <http://www.cookbook-r.com/Graphs/>`_
- Report generation: `knitr <https://yihui.name/knitr/>`_
- Manipulate tables in R: `dplyr <https://cran.rstudio.com/web/packages/dplyr/vignettes/introduction.html>`_
